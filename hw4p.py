1#
string = 'Судья показывает на аут';input().split()# Ввод
best = 'аут' * 3 # Самая короткая строка
vowels = 'aeiouуеыаоуэёяию' # Английские и русские гласные
for new_string in string:
    # Проходимся по списку и проверяем каждую пару символов (абв = аб бв)
    for i in range(len(new_string)-1):
        check_string = new_string[i:i+2]
        if check_string[0] in vowels and check_string[1] in vowels: # Проверяем является ли пара гласными
            if len(new_string) < len(best): # Если строка короче лучшей делаем её лучшей строкой
                best = new_string
print(best)

#2
lower_limit = 35.9
upper_limit = 37.3
data = {"citrus": 47.999, "istudio": 42.999, "moyo": 49.999, "royal-service": 37.245, "buy.ua": 38.324, "g-store": 37.166, "ipartner": 38.988, "sota": 37.720, "rozetka": 38.003}
for i in data:
    if data[i] > lower_limit and data[i] < upper_limit:
        print(i)
