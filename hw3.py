#1
string = 'The 3 symbol in "Project" is '
symbol = '"o"'
new_string = '{0}symbol{1}'.format(string, symbol)
print(new_string)
#2
string = (input('Введите строку из слов,в которой содержится слова,заканчивающееся на букву "o" '))
list_first = []
i = 0
while i < len(string) and string[i] != ":":
    word = ""
    while i < len(string) and string[i] != ";" and string[i] != "," and string[i] != ":":
        word += string[i]
        i = i + 1
    list_first.append(word)
    i = i + 2
x = 0
for word in list_first:
    if word[len(word) - 1] == 'о' or 'О':
        x = x + 1
        print(x)
#3
lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']

lst2 = [p for k, p in enumerate(lst1) if k not in [2, 3, 5, 7, 8, 11, 12]]

print(lst2)
