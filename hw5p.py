 #1
def calc(a, b):
    try:
        a = a + 0 # Если a строка то будет ошибка
        a_is_integer = True # Если a число
    except:
        a_is_integer = False # Если a строка
    try:
        b = b + 0
        b_is_integer = True
    except:
        b_is_integer = False
    if a_is_integer and b_is_integer:
        return a * b
    elif not a_is_integer and not b_is_integer:
        return a + b
    elif not a_is_integer and b_is_integer:
        return {a: b}
    else:
        return (a, b)


#2
def calc(string):
    data = {'0': string.count(' '), 'punctuation': ()}
    puncts = '!?.,:;'
    new_string = ''
    for i in string:
        if i in puncts:
            data['punctuation'] = data['punctuation'] + (i,)
        else:
            new_string += i

    new_string = new_string.split()
    for i in new_string:
        try:
            data[str(len(i))].append(i)
        except:
            data[str(len(i))] = [i]

    data['punctuation'] = set(data['punctuation'])
    data['punctuation'] = tuple(data['punctuation'])

    return data